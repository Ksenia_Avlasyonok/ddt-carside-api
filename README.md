# DDT carside API

## Source
`git clone git@bitbucket.org:Ksenia_Avlasyonok/ddt-carside-api.git`


## Build
- Windows `gradlew.bat build` or `gradlew.bat bootJar`
- Other `./gradlew build` or `./gradlew bootJar`


#### COMMAND LINE PARAMETERS
## RUN
    --port           Server port, default: 8084
    --host           Server host, default: localhost
    --logs.dir       Directory with Kapp logs [REQUIRED]
    --db.host        Database host, default: localhost
    --db.port        Database port, default: 1522
    --db.username    Database username, default: KVDEV
    --db.password    Database password, default: KYRIBA


## RUN
    java -jar <path/to/jar> --logs.dir=<path/to/logs>

    curl -v <host>:<port>/v1/logs --output logs.zip       Download all Logs
    curl -X DELETE <host>:<port>/v1/logs                  Delete all Logs
    curl -X DELETE <host>:<port>/v1/taskmonitor           Clean task monitor