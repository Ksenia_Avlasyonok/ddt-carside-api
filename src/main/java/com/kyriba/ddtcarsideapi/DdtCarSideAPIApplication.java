package com.kyriba.ddtcarsideapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author M-KAV
 */
@SpringBootApplication
public class DdtCarSideAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdtCarSideAPIApplication.class, args);
	}

}
