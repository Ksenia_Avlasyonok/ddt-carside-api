package com.kyriba.ddtcarsideapi.controller;

import com.kyriba.ddtcarsideapi.provider.LogsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author M-KAV
 */
@RestController
public class LogRestController {

    private final LogsProvider logsProvider;

    @Autowired
    public LogRestController(LogsProvider logsProvider) {
        this.logsProvider = logsProvider;
    }


    @GetMapping(value = "/v1/logs", produces = "application/zip")
    public @ResponseBody ResponseEntity<Resource> downloadLogs() {
        byte[] data;
        try {
            data = logsProvider.getLogs().toByteArray();
            ByteArrayResource resource = new ByteArrayResource(data);
            return ResponseEntity.ok()
                    .cacheControl(CacheControl.noStore())
                    .contentType(MediaType.parseMediaType("application/zip"))
                    .contentLength(data.length)
                    .body(resource);
        } catch (IOException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    @DeleteMapping("/v1/logs")
    public @ResponseBody ResponseEntity<String> deleteLogs() {
        try {
            logsProvider.cleanLogsDirectory();
            return new ResponseEntity<>("All logs were deleted", HttpStatus.OK);
        } catch (IOException exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}

