/********************************************************************************
 * Copyright 2021 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.ddtcarsideapi.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author M-KAV
 */
@Component
public class LogsProvider {
    @Value("${logs.dir}")
    String logsDir;


    @Autowired
    public LogsProvider() {
    }


    public void cleanLogsDirectory() throws IOException {
        clearAllLogsFileContent();
        deleteAllNonLogsFiles();
    }


    public ByteArrayOutputStream getLogs() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(baos)) {
            addDirToZipArchive(zos, new File(logsDir), null);
        }
        return baos;
    }


    private void clearAllLogsFileContent() throws IOException {
        List<String> filePathsToBeCleaned = collectAllPathsToLogsFiles();

        for (String fileName : filePathsToBeCleaned) {
            try (FileWriter fileWriter = new FileWriter(fileName, false)) {
                fileWriter.write("");
            }
        }
    }


    private List<String> collectAllPathsToLogsFiles() throws IOException {
        try (Stream<Path> walk = Files.walk(Path.of(logsDir))) {
            return walk
                    .filter(path -> !Files.isDirectory(path))
                    .map(path -> path.toString().toLowerCase())
                    .filter(fileName -> fileName.endsWith("log"))
                    .collect(Collectors.toList());
        }
    }


    private void deleteAllNonLogsFiles() throws IOException {
        List<String> filePathsToBeDeleted = collectAllPathsToNonLogsFiles();

        for (String fileName: filePathsToBeDeleted){
            Files.delete(Path.of(fileName));
        }
    }


    private List<String> collectAllPathsToNonLogsFiles() throws IOException {
        try (Stream<Path> walk = Files.walk(Path.of(logsDir))) {
            return walk.filter(path -> !Files.isDirectory(path))
                    .map(path -> path.toString().toLowerCase())
                    .filter(filename -> !filename.endsWith("log"))
                    .collect(Collectors.toList());
        }
    }


    private void addDirToZipArchive(ZipOutputStream zos, File fileToZip, String parentDirectoryName) throws IOException {
        if (fileToZip == null || !fileToZip.exists()) {
            return;
        }

        String zipEntryName = fileToZip.getName();
        if (parentDirectoryName != null && !parentDirectoryName.isEmpty()) {
            zipEntryName = parentDirectoryName + "/" + fileToZip.getName();
        }

        File[] files = fileToZip.listFiles();
        if (files != null && fileToZip.isDirectory()) {
            for (File file : files) {
                addDirToZipArchive(zos, file, zipEntryName);
            }
        } else {
            byte[] buffer = new byte[1024];
            try (FileInputStream fis = new FileInputStream(fileToZip)) {
                zos.putNextEntry(new ZipEntry(zipEntryName));
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
            }
        }
    }
}